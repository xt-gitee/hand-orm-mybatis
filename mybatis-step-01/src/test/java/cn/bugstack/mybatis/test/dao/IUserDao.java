package cn.bugstack.mybatis.test.dao;

/**
 * @Name: IUserDao
 * @Description:
 * @Author: Mr.Tong
 */
public interface IUserDao {

    String queryUserName(String uId);

    Integer queryUserAge(String uId);

}
